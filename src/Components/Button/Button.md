**Isolated React component development environment with a living style guide**

Button example:

```js
<Button variant="dark">Dark variant</Button>
<Button variant="light">Light variant</Button>
```
