import React from 'react';
import PropTypes from 'prop-types'
import './Button.scss';

/**
 * General component fon Buttons, Links, Anchors.
 */
export const Button = ({
    variant,
    children
}) => {
    let style = {};
    if(variant ==='light'){
        style = {
            color: '#333333',
            background: '#eeeeee',
        };
    }
    if(variant ==='dark'){
        style = {
            color: '#eeeeee',
            background: '#333333',
        };
    }
    return (
        <button
            className={`button--${variant}`}
            style={style}
        >
            {children}
        </button>
    );
};

Button.propTypes = {
    variant: PropTypes.string
};

Button.defaultProps = {
    variant: "light"
};

export default Button;
