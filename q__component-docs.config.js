import path from 'path';

module.exports = {
    port: 3003,
    pages: [
      { type: 'md', file: path.resolve(__dirname, 'index.md') },
      { type: 'md', file: path.resolve(__dirname, 'guide.md') },
    ],
  };
